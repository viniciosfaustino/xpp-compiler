/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package compiler;

/**
 *
 * @author Vinicios
 */
public class Token 
{
    public Enum name;
    public Enum attribute;
    public String lexeme;
    public int lineNumber;
    //public STEntry tsPtr;
    
    public Token(Enum name)
    {
        this.name = name;
        attribute = Enum.UNDEF;
        lineNumber = -1;
        //tsPtr = null;
    }
    
    public Token(Enum name, Enum attr)
    {
        this.name = name;
        attribute = attr;
        //tsPtr = null;
    }
}
