/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package compiler;

import java.io.IOException;

/**
 *
 * @author vinicios
 */

public class Parser 
{
    private Scanner scan;
    private Token lToken;
    
    private SymbolTable globalST;
    private SymbolTable currentST;
    
    public String previousLexeme;
    public String oldType;
    public String currentType;
    public String typeName;
    public boolean type;
    
    private String exception;
    private String semanticException;        
    
    public Parser(String inputFileName) throws IOException    
    {
        //Instancia a tabela de símbolos global e a inicializa
        globalST = new SymbolTable<STEntry>();
        initSymbolTable();
     
        //Faz o ponteiro para a tabela do escopo atual apontar para a tabela global
        currentST = globalST;
        
        //Instancia o analisador léxico
        scan = new Scanner(globalST, inputFileName);
        exception = "Erros: ";
        semanticException = "Erros: ";
        oldType = "";
        type = true;
    }
    
    /*
     * Método que inicia o processo de análise sintática do compilador
     */
    public void execute()
    {        
        advance();
        
        try
        {
            program();
        }
        catch(CompilerException e)
        {
            System.err.println(e);
        }
    }
    
    private void advance()
    {
        lToken = scan.nextToken();
        setException(scan.errors);
        while (lToken.name == Enum.LINEEND || lToken.name == Enum.COMMENT)
        {
            lToken = scan.nextToken();
        }
        
        //muda o name do token para palavras reservadas
        if (lToken.name == Enum.ID && this.globalST.isAdded(lToken))
        {
            STEntry st = this.globalST.get(lToken.lexeme);
            if (st.reserved)
            {
                lToken.name = st.token.name;
            }
        }               
    }
    
    private void match(Enum cTokenName) throws CompilerException
    {
        if (lToken.name == cTokenName || lToken.attribute == cTokenName)
            advance();
        else
        {            //Erro
            if (getExceptions().equals("Erros: ")) this.setException("Erro sintático! Token inesperado! Lexema:" + lToken.lexeme + ".Linha:" +scan.getLineNumber() +".\n O esperado era "+cTokenName+ ".\n");
        }
    }
    
    /*
     * Método para o símbolo inicial da gramática
     */    
    
    //01
    private void program() throws CompilerException
    {        
        if (lToken.name == Enum.CLASS)
        {            
            classList();               
        }else if (lToken.name != Enum.EOF){
            this.exception = "Classe mal definida! Linha: "+lToken.lineNumber + " token encontrado: "+lToken.lexeme;
        }
//        System.out.println("terminei, brother");
        if (getExceptions().equals("Erros: ") && getSemanticException().equals("Erros: "))
        {
            this.exception = "Compilado com sucesso!";
            this.semanticException = "";
        }
        if (getExceptions().equals("Erros: "))
        {
            this.exception = "";
        }
        
        if (getSemanticException().equals("Erros: "))
        {
            this.semanticException = "";
        }
    }
    
    //02
    private void classList() throws CompilerException
    {
        while (lToken.name == Enum.CLASS)
        {
            classDecl();
        }        
    }
    
    //3
    private void classDecl() throws CompilerException
    {
        if (lToken.name == Enum.CLASS)
        {            
            advance();
            
            if (lToken.name == Enum.ID)
            {   
                Token t = new Token(Enum.CLASS);
                t.lexeme = lToken.lexeme;
                STEntry st = new STEntry(t, lToken.lexeme);
                insertSymbolTable(st);
                
                advance();
                
                if (lToken.name == Enum.EXTENDS)
                {
                    advance();                                        
                    
                    if (lToken.name == Enum.ID)
                    {
                        if (!currentST.isAdded(lToken))
                        {
                            if (getExceptions().equals("Erros: ")) this.setSemanticException("Erro semântico! Variável '"+lToken.lexeme +"' não declarada" + " Linha: " + scan.getLineNumber() + "\n");
                        }                         
                        
                        advance();
                    }
                    else
                    {                        
                        if (getExceptions().equals("Erros: ")) this.setException("Erro Sintático! ID. Linha:" +scan.getLineNumber() + "\n");
                    }
                }
                classBody();
            }
            else
                if (getExceptions().equals("Erros: ")) this.setException("Erro Sintático! ID esperado. Linha:" +scan.getLineNumber() + "\n");
        }
        else
            if (getExceptions().equals("Erros: ")) this.setException("Classe mal definida. Linha:" +scan.getLineNumber() + "\n");
        
    }
        
    //pi eh um numero ou uma letra ou plano ou o teto    
    private void varMethDeclListOpt() throws CompilerException
    {
        if (isType(lToken.name) || isType(lToken))
        {
            
            typeName = lToken.lexeme;
            advance();
            if (lToken.attribute == Enum.LBRA)
            {
                advance();
                if (lToken.attribute == Enum.RBRA)
                {
                    advance();
                }
                else
                {
                    if (getExceptions().equals("Erros: ")) this.setException("Erro Sintático! ']' esperado. Linha:" +scan.getLineNumber() + "\n"); 
                }
            }
            if (lToken.name == Enum.ID)
            {
                previousLexeme = lToken.lexeme;                
                advance();
                if (lToken.attribute == Enum.LPAR)
                {
                    Token t = new Token(Enum.MET);
                    t.lexeme = typeName;
                    STEntry st = new STEntry(t, previousLexeme);
                    insertSymbolTable(st);
                    methodBody();
                    methodDeclListOpt();
                    if (lToken.name == Enum.CONSTRUCTOR)
                    {
                        if (getExceptions().equals("Erros: ")) this.setException("Erro sintático! Construtor deveria estar antes dos métodos. Linha:" +scan.getLineNumber() + "\n");
                    }
                }
                else
                {
                    Token t = new Token(Enum.VAR);
                    t.lexeme = typeName;
                    STEntry st = new STEntry(t, previousLexeme);
                    insertSymbolTable(st);
                    varDeclOpt();                    
                    if (lToken.attribute == Enum.SCO)
                    {
                        advance();
                        varMethDeclListOpt();
                        constructDeclListOpt();
                        methodDeclListOpt();
                    }
                    else{
                        if (getExceptions().equals("Erros: ")) this.setException("Erro sintático! ';' esperado. Token atual:"+lToken.attribute+ " Linha:" +scan.getLineNumber() + "\n");
                    }
                }                
            }
        }
        else
        {
            if (lToken.name != Enum.CONSTRUCTOR && !(lToken.name == Enum.SEP && lToken.attribute == Enum.RCBRA))
                if (getExceptions().equals("Erros: ")) this.setException("Erro sintático! Tipo inválido na declaração! Linha:" +scan.getLineNumber() + "\n");
        }
    }
    
    //4 Checar ambiguidade nas chamadas dos 3 métodos dentro das chaves
    private void classBody() throws CompilerException
    {        
        currentST = new SymbolTable<STEntry>(currentST);
                
        if (lToken.attribute == Enum.LCBRA){
            advance();            
            if (lToken.name == Enum.CONSTRUCTOR)
            {
                constructDeclListOpt();
                methodDeclListOpt();
            }
            else
            {
                varMethDeclListOpt();
            }
            if (lToken.attribute == Enum.RCBRA){                
                advance();
            }
            else
            {
                if (getExceptions().equals("Erros: ")) this.setException("Erro sintático! '}' esperado. Token atual:"+lToken.attribute+ " Linha:" +scan.getLineNumber() + "\n");

            }                
        }
        else
            if (getExceptions().equals("Erros: ")) this.setException("Erro sintático! '{' esperado. Linha:" +scan.getLineNumber() + "\n");

        currentST = currentST.parent;
    }
    
    //5
    private void varDeclListOpt() throws CompilerException
    {
        if (lToken.name != Enum.CONSTRUCTOR && lToken.attribute != Enum.RCBRA)
        {
            varDeclList();
        }
        else ;
    }
    
    //6
    private void varDeclList() throws CompilerException
    {
        varDecl();
        while(isType(lToken.name) || isType(lToken))
        {
            varDecl();
        }
    }
    
    //7
    private void varDecl() throws CompilerException
    {
        previousLexeme = lToken.lexeme;
        type();
        if (lToken.attribute == Enum.LBRA)
        {
            advance();
            match(Enum.RBRA);
        } 
        if (lToken.name == Enum.ID)
        {
            Token t = new Token(Enum.VAR);
            t.lexeme = typeName;
            STEntry st = new STEntry(t, lToken.lexeme);
            insertSymbolTable(st);
            advance();
            varDeclOpt();
        }
        else
        {
            if (getExceptions().equals("Erros: ")) this.setException("Erro sintático! ID esperado. Linha:" +scan.getLineNumber() + "\n");            
        }
        match(Enum.SCO);
    }
    
    //8
    private void varDeclOpt() throws CompilerException
    {
        if (lToken.attribute == Enum.COM)
        {
            advance();
            if(lToken.name == Enum.ID)
            {
                Token t = new Token(Enum.VAR);
                t.lexeme = typeName;
                STEntry st = new STEntry(t, lToken.lexeme);
                insertSymbolTable(st);
                advance();
                varDeclOpt();
            } 
            else
            {                
                if (getExceptions().equals("Erros: ")) this.setException("Erro Sintático! ID esperado. Linha:" +scan.getLineNumber() + "\n");
            }
        }
        else ;
    }
    
    //9
    private void type() throws CompilerException
    {
        if (isType(lToken.name) || isType(lToken))
        {
            typeName = lToken.lexeme;
            advance();
        }else
            if (getExceptions().equals("Erros: ")) this.setException("Tipo inválido! Linha:" +scan.getLineNumber() + "\n");

    }
    
    //10
    private void constructDeclListOpt() throws CompilerException
    {
        if (!(isType(lToken.name) || isType(lToken)) && lToken.name != Enum.SEP)
        {
            constructDeclList();
        }
        else ;
    }
    
    //11
    private void constructDeclList() throws CompilerException
    {
        constructDecl();
        while(lToken.name == Enum.CONSTRUCTOR)
        {
            constructDecl();
        }
    }
    
    //12 Checar com professora uma mensagem de exceção
    private void constructDecl() throws CompilerException
    {
        if (lToken.name == Enum.CONSTRUCTOR)
        {
            advance();
            methodBody();
        }
        else
        {
            if (getExceptions().equals("Erros: ")) this.setException("Construtor inválido! Linha:" +scan.getLineNumber() + "\n");
        }
    }
    
    //13
    private void methodDeclListOpt() throws CompilerException
    {
        if (lToken.attribute != Enum.RCBRA)
        {
            methodDeclList();
        }
        else ;
        
    }
    
    //14
    private void methodDeclList() throws CompilerException
    {
        methodDecl();
        while (isType(lToken.name) || isType(lToken))
        {
            methodDecl();
        }
    }
    
    //15
    private void methodDecl() throws CompilerException
    {
        if (isType(lToken.name) || isType(lToken))
        {
            typeName = lToken.lexeme;
            advance();
            if (lToken.attribute == Enum.LBRA)
            {
                advance();
                if (lToken.attribute == Enum.RBRA)
                {
                    advance();
                }
                else
                {
                    if (getExceptions().equals("Erros: ")) this.setException("Erro sintático! ']' esperado. Linha:" +scan.getLineNumber() + "\n");
                }
            }
            
            if (lToken.name == Enum.ID)
            {
                Token t = new Token(Enum.MET);
                STEntry st = new STEntry(t, lToken.lexeme);
                insertSymbolTable(st);
                advance();
                methodBody();
            }
            else
                if (getExceptions().equals("Erros: ")) this.setException("Declaração de método inválida! Linha:" +scan.getLineNumber() + "\n");
            
        }
        else
            if (getExceptions().equals("Erros: ")) this.setException("Tipo inválido na declaração de metodo! Linha:" +scan.getLineNumber() + "\n");
    }
    
    //16
    private void methodBody() throws CompilerException{
        currentST = new SymbolTable<STEntry>(currentST);
         if (lToken.attribute == Enum.LPAR){
             advance();
             paramListOpt();
             if(lToken.attribute == Enum.RPAR){
                 advance();
                 if(lToken.attribute == Enum.LCBRA){
                     advance();
                     statementsOpt();
                     match(Enum.RCBRA);
                 
                 }else{
                     if (getExceptions().equals("Erros: ")) this.setException("Erro sintático! '{' esperado na declaração de método. Linha:" +scan.getLineNumber() + "\n");
                 
                 }
                              
             }else{
                 if (getExceptions().equals("Erros: ")) this.setException("Erro sintático! ')' esperado na declaração de método. Linha:" +scan.getLineNumber() + "\n");
             }
         
         }else{
         if (getExceptions().equals("Erros: ")) this.setException("Erro sintático! '(' esperado na declaração de método. Linha:" +scan.getLineNumber() + "\n");
         
         }
         currentST = currentST.parent;
                   
    }
    
    //17
    private void paramListOpt() throws CompilerException{
        if(lToken.attribute != Enum.RPAR){
            paramList();        
        }else{
            ;
        }
    }
    
    //18
    private void paramList() throws CompilerException
    {
        param();
        while(lToken.attribute == Enum.COM){
            advance();
            param();
        }        
    }
    
    //19
    private void param() throws CompilerException
    {
        if(isType(lToken.name) || isType(lToken)){
           typeName = lToken.lexeme;
           advance();
           if(lToken.attribute == Enum.LBRA)
           {
               advance();
               match(Enum.RBRA);
           }
           if(lToken.name == Enum.ID)
           {
               Token t = new Token(Enum.VAR);
               t.lexeme = typeName;
               STEntry st = new STEntry(t, lToken.lexeme);
               insertSymbolTable(st);
               advance();
           } 
           else if (getExceptions().equals("Erros: ")) this.setException("Erro sintático! ID esperado. Linha:" +scan.getLineNumber() + "\n");
        } 
        else if (getExceptions().equals("Erros: ")) this.setException("Tipo inválido. Linha:" +scan.getLineNumber() + "\n");
    }
    
    //20 Checar todos os não-terminais "opt" se estamos checando o caractere pós-vazio ou não
    private void statementsOpt() throws CompilerException
    {
        if(lToken.attribute != Enum.RCBRA)
        {
            statements();
        }
        else {;};
    }
    
    //21
    private void statements() throws CompilerException
    {
     statement();
     while (isStatement(lToken))
     {
         statement();
     }
    }
    
    //22
    private void statement() throws CompilerException
    {
        
        if(isType(lToken.name) || isType(lToken))
        {
            varDeclList();
        }
        else if(lToken.name == Enum.ID)
        {
            atribStat();            
            match(Enum.SCO);
        }
        else if(lToken.name == Enum.PRINT)
        {
            type = false;
            printStat();
            type = true;
            match(Enum.SCO);
        }
        else if(lToken.name == Enum.READ)
        {
            readStat();
            match(Enum.SCO);
        }
        else if(lToken.name == Enum.RETURN)
        {
            type = false;
            returnStat();
            type = true;
            match(Enum.SCO);
        }
        else if(lToken.name == Enum.SUPER)
        {
            type = false;
            superStat();
            type = true;
            match(Enum.SCO);
        }
        else if(lToken.name == Enum.IF)
        {
            ifStat();
        }
        else if(lToken.name == Enum.FOR)
        {
            forStat();
        }
        else if(lToken.name == Enum.BREAK)
        {
            advance();
            match(Enum.SCO);
        }
        else if(lToken.attribute == Enum.SCO)
        {
            match(Enum.SCO);
        }
        else if (getExceptions().equals("Erros: ")) this.setException("Declaração inválida. Linha:" +scan.getLineNumber() + "\n");
    }
    
    //23
    private void atribStat() throws CompilerException{        
        if(lToken.name == Enum.ID){
            if (currentST.isAdded(lToken) || currentST.parent.isAdded(lToken))
            {
                STEntry st = currentST.get(lToken.lexeme);
                oldType = st.token.lexeme;
            }
            else if (getExceptions().equals("Erros: ")) this.setSemanticException("Erro semântico! Variável '"+lToken.lexeme +"' não declarada" + " Linha: " + scan.getLineNumber() + "\n");
            lValue();
            if(lToken.attribute == Enum.AT){
                advance();                
                if((lToken.attribute == Enum.SUB) || (lToken.attribute == Enum.SUM)){                    
                    expression();
                }else{
                    if((lToken.name == Enum.NEW) || isType(lToken.name) || isType(lToken)){
                        allocExpression();
                    }else{
                        if (getExceptions().equals("Erros: ")) this.setException("Erro sintático! Atribuição mal declarada. Linha:" +scan.getLineNumber() + "\n");
                    }
                }
            }else{
                if (getExceptions().equals("Erros: ")) this.setException("Erro sintático! '=' esperado. Linha:" +scan.getLineNumber() + "\n");
            }
        }else{
            if (getExceptions().equals("Erros: ")) this.setException("Erro sintático! ID esperado. Linha:" +scan.getLineNumber() + "\n");
        }
    }
    
    //24
    private void printStat() throws CompilerException{
        if(lToken.name == Enum.PRINT){
            advance();
            expression();                        
        }else{
            if (getExceptions().equals("Erros: ")) this.setException("Erro sintático! 'print' esperado. Linha:" +scan.getLineNumber() + "\n");
        }        
    }
    
    //25
    private void readStat() throws CompilerException{
        if(lToken.name == Enum.READ){
            advance();
            lValue();                        
        }else{
            if (getExceptions().equals("Erros: ")) this.setException("Erro sintático! 'read' esperado. Linha:" +scan.getLineNumber() + "\n");
        }        
    }
    
    //26
    private void returnStat() throws CompilerException{
        if(lToken.name == Enum.RETURN){
            advance();
            expression();                       
        }else{
            if (getExceptions().equals("Erros: ")) this.setException("Erro sintático! 'return' esperado. Linha:" +scan.getLineNumber() + "\n");
        }        
    }
    
    //27
    private void superStat() throws CompilerException{
        if(lToken.name == Enum.SUPER){
            advance();
            if(lToken.attribute == Enum.LPAR){
                advance();
                type = false;
                argListOpt();
                type = true;
                match(Enum.RPAR);            
            }else{
                if (getExceptions().equals("Erros: ")) this.setException("Erro sintático! '(' esperado. Linha:" +scan.getLineNumber() + "\n");
            }
        }else{
            if (getExceptions().equals("Erros: ")) this.setException("Erro sintático! 'super' esperado. Linha:" +scan.getLineNumber() + "\n");
        }
        
    }
    
    //28
    private void ifStat() throws CompilerException{
        if(lToken.name == Enum.IF){
            advance();
            if(lToken.attribute == Enum.LPAR){
                advance();

                expression();
                if(lToken.attribute == Enum.RPAR){
                    advance();
                    if(lToken.attribute == Enum.LCBRA){
                        advance();
                        statements();
                        match(Enum.RCBRA);
                        if(lToken.name == Enum.ELSE){
                            advance();
                            if(lToken.attribute == Enum.LCBRA){
                                advance();
                                statements();
                                match(Enum.RCBRA);
                            }else{
                                if (getExceptions().equals("Erros: ")) this.setException("Erro sintático! '{' esperado. Linha:" +scan.getLineNumber() + "\n");
                            }
                        }else{
                            ;
                        }

                    }else{
                        if (getExceptions().equals("Erros: ")) this.setException("Erro sintático! '{' esperado. Linha:" +scan.getLineNumber() + "\n");

                    }
                }else{
                   if (getExceptions().equals("Erros: ")) this.setException("Erro sintático! ')' esperado.b Linha:" +scan.getLineNumber() + "\n");

                }
            }else{
                if (getExceptions().equals("Erros: ")) this.setException("Erro sintático! '(' esperado. Linha:" +scan.getLineNumber() + "\n");


            }
        }else{
            if (getExceptions().equals("Erros: ")) this.setException("Erro sintático!'if' esperado. Linha:" +scan.getLineNumber() + "\n");

        }

    }

    //29
    private void forStat() throws CompilerException{
        if(lToken.name == Enum.FOR){
            advance();
            if(lToken.attribute == Enum.LPAR){
                advance();
                atribStatOpt();
                if(lToken.attribute == Enum.SCO){
                    advance();                    
                    expressionOpt();
                    if(lToken.attribute == Enum.SCO){
                        advance();
                        atribStatOpt();
                        if(lToken.attribute == Enum.RPAR){
                            advance();
                            if(lToken.attribute == Enum.LCBRA){
                                advance();
                                statements();
                                match(Enum.RCBRA);
                            }else{
                                if (getExceptions().equals("Erros: ")) this.setException("Erro sintático! '{' esperado. Linha:" +scan.getLineNumber() + "\n");
                            }
                        }else{
                            if (getExceptions().equals("Erros: ")) this.setException("Erro sintático! ')' esperado. Linha:" +scan.getLineNumber() + "\n");
                        }    
                    }else{
                        if (getExceptions().equals("Erros: ")) this.setException("Erro sintático! ';' esperado. Linha:" +scan.getLineNumber() + "\n");
                    }                    
                }else{
                    if (getExceptions().equals("Erros: ")) this.setException("Erro sintático! ';' esperado. Linha:" +scan.getLineNumber() + "\n");
                }   
            }else{
                if (getExceptions().equals("Erros: ")) this.setException("Erro sintático! '(' esperado. Linha:" +scan.getLineNumber() + "\n");
            }        
        }else{
            if (getExceptions().equals("Erros: ")) this.setException("Erro sintático! Estrutura de repetição esperada. Linha:" +scan.getLineNumber() + "\n");       
        }
    }
    //30
    private void atribStatOpt() throws CompilerException
    {
        if(lToken.attribute != Enum.SCO)
        {
            atribStat();
        }
        else ;
    }
    
    //31
    private void expressionOpt() throws CompilerException
    {
        if(lToken.attribute != Enum.SCO)
        {
            expression();
        }
        else ;
    }
    
    //32
    private void lValue() throws CompilerException
    {        
        if (lToken.name == Enum.ID)
        {   
            advance();
            if(lToken.attribute == Enum.LBRA)
            {
                advance();
                type = false;
                expression();
                type = true;
                match(Enum.RBRA);              
            }
            lValueComp();
        }
        else if (getExceptions().equals("Erros: ")) this.setException("Erro sintático! ID esperado! Linha:" +scan.getLineNumber() + "\n");
    }
    
    //33
    private void lValueComp() throws CompilerException
    {
        if(lToken.attribute == Enum.DOT)
        {
            advance();
            if(lToken.name == Enum.ID)
            {
                advance();
                if(lToken.attribute == Enum.LBRA)
                {
                    advance();
                    expression();
                    match(Enum.RBRA);
                }                                
                lValueComp();
            }
        }
        else ;
    }
    
    //34
    private void expression() throws CompilerException
    {        
        numExpression();    
        if(lToken.attribute == Enum.GT ||
                lToken.attribute == Enum.LT ||
                lToken.attribute == Enum.EQ ||
                lToken.attribute == Enum.GE || 
                lToken.attribute == Enum.LE ||
                lToken.attribute == Enum.NE )
        {
            advance();
            numExpression();
        }
    }
    
    //35
    private void allocExpression() throws CompilerException
    {
        if(lToken.name == Enum.NEW)
        {
            advance();
            if(lToken.name == Enum.ID)
            {
                if (!(isAdded(lToken) || isAddedGlobal(lToken)))    
                    if (getExceptions().equals("Erros: ")) this.setSemanticException("Erro semântico! Nome '"+lToken.lexeme +"' não declarada" + " Linha: " + scan.getLineNumber() + "\n");
                
                advance();
                if(lToken.attribute == Enum.LPAR)
                {
                    advance();
                    argListOpt();
                    match(Enum.RPAR);
                } else if (getExceptions().equals("Erros: ")) this.setException("Erro sintático! '(' esperado. Linha:" +scan.getLineNumber() + "\n");
            } else if (getExceptions().equals("Erros: ")) this.setException("Erro sintático! ID esperado!!. Linha:" +scan.getLineNumber() + "\n");
        }
        else
        {
            if(isType(lToken.name) || isType(lToken))
            {
                advance();
                if (lToken.attribute == Enum.LBRA)
                {
                    advance();
                    type = false;
                    expression();
                    type = true;
                    match(Enum.RBRA);
                } else if (getExceptions().equals("Erros: ")) this.setException("Erro sintático!'[' esperado. Linha:" +scan.getLineNumber() + "\n");
            } else if (getExceptions().equals("Erros: ")) this.setException("Alocação mal definida. Linha:" +scan.getLineNumber() + "\n");
        }
    }
    
    //36
    private void numExpression() throws CompilerException
    {
        term();        
        if(lToken.attribute == Enum.SUM || lToken.attribute == Enum.SUB)
        {
            advance();
            term();
        }
        else ;
    }
    
    //37
    private void term() throws CompilerException
    {
        unaryExpression();
        if(lToken.attribute == Enum.MUL || lToken.attribute == Enum.DIV || lToken.attribute == Enum.MOD)
        {
            advance();
            unaryExpression();
        }
        else ;
    }
    
    //38
    private void unaryExpression() throws CompilerException
    {
        if(lToken.attribute == Enum.SUM || lToken.attribute == Enum.SUB)
        {
            advance();
            factor();
        }
        else if (getExceptions().equals("Erros: ")) this.setException("Erro sintático! Sinal (+ ou -) esperado. Linha:" +scan.getLineNumber() + "\n");
    }
    
    //39 CHECAR!!!
    private void factor() throws CompilerException
    {           
        if (lToken.name == Enum.INTEGER_LITERAL || lToken.name == Enum.STRING_LITERAL)
        {
            if(lToken.name == Enum.INTEGER_LITERAL)
            {
                currentType = "int";
            }
            else
            {
                currentType = "string";
            }
            verifyType();
            
            advance();
        }
        else
        {
            if(lToken.attribute == Enum.LPAR)
            {
                advance();
                expression();
                match(Enum.RPAR);
            }
            else
            {
                if(lToken.name == Enum.ID)
                {
                    if (currentST.isAdded(lToken) || currentST.parent.isAdded(lToken))
                    {
                        STEntry st = currentST.get(lToken.lexeme);
                        currentType = st.token.lexeme;
                        verifyType();
                    }
                    else
                    {
                        if (getExceptions().equals("Erros: ")) this.setSemanticException("Erro semântico! Variável '"+lToken.lexeme +"' não declarada" + " Linha: " + scan.getLineNumber() + "\n");
                    }
                    lValue();                    
                }
                else if (getExceptions().equals("Erros: ")) this.setException("Fator mal definido. Linha:" +scan.getLineNumber() + "\n");
            }
        }
    }
    
    //40
    private void argListOpt() throws CompilerException
    {
        if(lToken.attribute != Enum.RPAR)
        {
            type = false;
            argList();
            type = true;
        }
        else ;
    }
    
    //41
    private void argList() throws CompilerException
    {
        type = false;        
        expression();        
        while(lToken.attribute == Enum.COM)
        {
            advance();
            expression();
        }
        type = true;
    } 

	//DEMAIS MÉTODOS....
    
    private void initSymbolTable()
    {
        Token t;
        
        t = new Token(Enum.CLASS);
        globalST.add(new STEntry(t, "class", true));
        t = new Token(Enum.EXTENDS);
        globalST.add(new STEntry(t, "extends", true));                
        t = new Token(Enum.CONSTRUCTOR);
        globalST.add(new STEntry(t, "constructor", true));
        t = new Token(Enum.INT);
        globalST.add(new STEntry(t, "int", true));
        t = new Token(Enum.STRING);        
        globalST.add(new STEntry(t, "string", true));        
        t = new Token(Enum.BREAK);
        globalST.add(new STEntry(t, "break", true));
        t = new Token(Enum.PRINT);
        globalST.add(new STEntry(t, "print", true));
        t = new Token(Enum.READ);
        globalST.add(new STEntry(t, "read", true));
        t = new Token(Enum.RETURN);
        globalST.add(new STEntry(t, "return", true));
        t = new Token(Enum.SUPER);
        globalST.add(new STEntry(t, "super", true));
        t = new Token(Enum.IF);
        globalST.add(new STEntry(t, "if", true));
        t = new Token(Enum.ELSE);
        globalST.add(new STEntry(t, "else", true));
        t = new Token(Enum.FOR);
        globalST.add(new STEntry(t, "for", true));
        t = new Token(Enum.NEW);
        globalST.add(new STEntry(t, "new", true));        
 
    }

    private boolean isType(Enum name) 
    {
        if ((name == Enum.INT || name == Enum.STRING) /*|| name == Enum.ID*/)
        {
            return true;
        }else
        {
            return false;
        }                
    }
    
    private boolean isStatement(Token token)
    {
        Enum name = lToken.name;
        if( isType(name) || isType(token)|| name == Enum.ID || name == Enum.PRINT || 
                name == Enum.READ || name == Enum.RETURN || name == Enum.SUPER || 
                name == Enum.IF || name == Enum.FOR || name == Enum.BREAK || name == Enum.SCO)
        {
            return true;
        }
        else
        {
            return false;
        }
    }  
    
    public boolean isType(Token token)
    {
        if (token.name == Enum.ID)
        {
            if (currentST.parent.isAdded(token))
            {
                STEntry st = currentST.parent.get(token.lexeme);
                if (st.token.name == Enum.CLASS)
                {
                    return true;
                }
            }
            SymbolTable t = currentST.parent;
            if (t.parent != null)
            {
                if (t.parent.isAdded(token))
                {
                    STEntry st = t.parent.get(token.lexeme);
                    if (st.token.name == Enum.CLASS)
                    {
                        return true;
                    }
                }                
            }
        }
        return false;
        
    }
        
    public void setException(String msg)
    {
        this.exception = this.exception + msg;
    }
    
    public String getExceptions()
    {
        return this.exception;
    }
    
    public String getSemanticException() {
        return semanticException;
    }

    public void setSemanticException(String semanticException) {
        this.semanticException = this.semanticException + semanticException;
    }
    
    public void verifyType()
    {   
        if (type)
        {
            if(currentType != null && !oldType.equals(""))
            {
                if(!currentType.equals(oldType))
                {
                    if (getExceptions().equals("Erros: ")) this.setSemanticException("Erro semântico! A operação contém tipos diferentes!" + " Linha: " + scan.getLineNumber() + "\n");
                }    
            }
        }
    }
    
    public boolean isAdded(Token token)
    {
        if (currentST.isAdded(token)) return true;
        if (currentST.parent.isAdded(token)) return true;        
        
        return false;
    }
    
    public boolean isAddedGlobal(Token token)
    {
        if (globalST.isAdded(token)) return true;
        return false;
    }
    
    public void setLTokenName(Enum name)
    {
        this.lToken.name = name;
    }
    
    public void insertSymbolTable(STEntry st)
    {
        if (!currentST.add(st))
        {
            if (getExceptions().equals("Erros: "))
            {
                this.setSemanticException("Erro semântico! Nome já existente!" + " Linha: " + scan.getLineNumber() + "\n");
            }
        }        
    }
}
