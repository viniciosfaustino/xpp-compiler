package compiler;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.CharacterIterator;
import java.text.StringCharacterIterator;
/**
 *
 * @author Vinicios
 */
public class Scanner 
{
    private static String input = "";
    private int pos;
    
    private SymbolTable st;
    private int lineNumber;
    
    public String errors = "";
    
    public Scanner(SymbolTable globalST, String inputFileName) throws FileNotFoundException, IOException
    {
        st = globalST;
        input = "";
        File inputFile = new File(inputFileName);               
        FileReader fr = new FileReader(inputFile);        
        BufferedReader reader = new BufferedReader(fr);        
        int p;
        while( (p = reader.read()) != -1){
            input += Character.toString((char)p) + reader.readLine() + "\n";
        }                    
        lineNumber = 1;
        
        
    }
    
    public Token nextToken()
    {
        int aux;
        aux = pos;
        Token tok = new Token(Enum.UNDEF);
        Substring sub = new Substring();
        int state = 0;        
        
        String lexema;
        sub.begin = sub.end = pos;
        
        if (pos == input.length())
            return tok = new Token(Enum.EOF);
        
        if (input.charAt(pos) == '\n')
        {                        
            setLineNumber();
        }
                       
        while (pos < input.length() && Character.isWhitespace(input.charAt(pos)) && (input.charAt(pos) != '\n'))
            pos++;
        
        if (input.charAt(pos) == '\n')
        {
            pos++;
            return tok = new Token(Enum.LINEEND);
        }
        
        //Trecho para reconhecer um OP        
        if (input.charAt(pos) == '>' || input.charAt(pos) == '<' || 
            input.charAt(pos) == '=' || input.charAt(pos) == '!' ||
            input.charAt(pos) == '+' || input.charAt(pos) == '-' ||
            input.charAt(pos) == '*' || input.charAt(pos) == '%')
        {            
            //É um OP
            sub.begin = pos;
            tok = new Token(Enum.OP);
            tok.lineNumber = lineNumber;
            //int state = 0;
            
            while (true)
            {
                switch (state)
                {
                    case 0:
                        if (input.charAt(pos) == '<')
                        {
                            pos++;
                            state = 1;
                        }
                        else if (input.charAt(pos) == '=')
                        {                            
                            pos++;
                            state = 4;
                        }
                        else if (input.charAt(pos) == '>')
                        {
                            pos++;
                            state = 7;
                        }
                        else if (input.charAt(pos) == '!')
                        {
                            pos++;
                            state = 10;
                        }
                        else if (input.charAt(pos) == '+')
                        {
                            pos++;
                            tok.attribute = Enum.SUM;
                            tok.lexeme = sub.willi(sub, pos, input);
                            return tok;
                        }
                        else if (input.charAt(pos) == '-')
                        {
                            pos++;
                            tok.attribute = Enum.SUB;
                            tok.lexeme = sub.willi(sub, pos, input);
                            return tok;
                        }
                        else if (input.charAt(pos) == '*')
                        {
                            pos++;
                            tok.attribute = Enum.MUL;
                            tok.lexeme = sub.willi(sub, pos, input);
                            return tok;
                        }
                        else if (input.charAt(pos) == '%')
                        {
                            pos++;
                            tok.attribute = Enum.MOD;
                            tok.lexeme = sub.willi(sub, pos, input);
                            return tok;
                        }                        
                        else
                            fail();                   

                        break;
                    case 1:
                        if(pos == input.length())
                        {
                            pos++;
                            tok.lexeme = sub.willi(sub, pos, input);
                            tok.attribute = Enum.LT;
                        }
                        else if (input.charAt(pos) == '=')
                        {
                            pos++;
                            tok.lexeme = sub.willi(sub, pos, input);
                            tok.attribute = Enum.LE;
                        }
                        else
                        {
                            pos++;
                            tok.lexeme = sub.willi(sub, pos, input);
                            tok.attribute = Enum.LT;
                        }
                        return tok;
                    case 2:
                        
                        return tok;
                    case 3:
                        pos --;
                        

                        return tok;
                    case 4:
                        if(pos == input.length())
                        {                            
                            state = 6;
                        }
                        else if (input.charAt(pos) == '=')
                        {
                            pos++;
                            state = 5;
                        }
                        else
                        {
                            pos++;
                            state = 6;
                        }
                    break;
                        
                    case 5:
                        tok.attribute = Enum.EQ;
                        tok.lexeme = sub.willi(sub, pos, input);
                        return tok;
                    case 6:
                        tok.attribute = Enum.AT;
                        tok.lexeme = sub.willi(sub, pos+1, input);
                        return tok;
                        
                    case 7:
                        if(pos == input.length())
                        {
                            tok.attribute = Enum.GT;
                            tok.lexeme = sub.willi(sub, pos, input);
                            return tok;
                        }
                        else if (input.charAt(pos) == '=')
                        {
                            pos++;                            
                            tok.attribute = Enum.GE;                          
                            tok.lexeme = sub.willi(sub, pos, input);
                            return tok;
                        }
                        else 
                        {
                            tok.attribute = Enum.GT;
                            tok.lexeme = sub.willi(sub, pos, input);
                            return tok;
                        }
                        
                    case 10:
                        if(pos == input.length())
                        {
                            state = 0;
                        }
                        else if (input.charAt(pos) == '=')
                        {
                            pos++;
                            tok.attribute = Enum.NE;
                            tok.lexeme = sub.willi(sub, pos, input);
                            return tok;
                        }
                        else
                        {
                            fail();
                        }
                    case 11:
                }//switch
            }//while
        }
        else if (Character.isLetter(input.charAt(pos)) || input.charAt(pos) == '_')
        {
            //Identificador
            state = 0;
            while (true)
            {
                switch(state)
                {
                    case 0:
                        if (Character.isLetter(input.charAt(pos)) || input.charAt(pos) == '_')
                        {
                            pos++;
                            state = 18;                        
                        }
                        else
                            fail();

                        break;
                    case 18:
                        while (pos < input.length() && (Character.isLetterOrDigit(input.charAt(pos)) || input.charAt(pos) == '_'))
                            pos++;

                        pos++;
                        state = 19;

                        break;
                    case 19:
                        pos--;
                        tok = new Token(Enum.ID);
                        tok.lexeme = sub.willi(sub, pos+1, input);
                        tok.lineNumber = lineNumber;
                        return tok;                   
                }
            }
        }
        else if (Character.isDigit(input.charAt(pos)))
        {
            // Números inteiros
           while(Character.isDigit(input.charAt(pos)))
           {
               pos++;
           }
           tok = new Token(Enum.INTEGER_LITERAL);           
           tok.lexeme = sub.willi(sub, pos+1, input);
           tok.lineNumber = lineNumber;
           return tok;
        }

        else if (input.charAt(pos) == '(' || input.charAt(pos) == ')' ||
                input.charAt(pos) == '[' || input.charAt(pos) == ']' ||
                input.charAt(pos) == '{' || input.charAt(pos) == '}' ||
                input.charAt(pos) == ';' || input.charAt(pos) == '.' ||
                input.charAt(pos) == ',')
        {
            //Separador
            
            tok = new Token(Enum.SEP);
            tok.lineNumber = lineNumber;
            if (input.charAt(pos) == '(')
            {
                pos++;
                tok.attribute = Enum.LPAR;
                tok.lexeme = sub.willi(sub, pos, input);
                return tok;
            }
            else if (input.charAt(pos) == ')')
            {
                pos++;
                tok.attribute = Enum.RPAR;
                tok.lexeme = sub.willi(sub, pos, input);
                return tok;
            }
            else if (input.charAt(pos) == '[')
            {
                pos++;
                tok.attribute = Enum.LBRA;
                tok.lexeme = sub.willi(sub, pos, input);
                return tok;
            }
            else if (input.charAt(pos) == ']')
            {
                pos++;
                tok.attribute = Enum.RBRA;
                tok.lexeme = sub.willi(sub, pos, input);
                return tok;
            }
            else if (input.charAt(pos) == '{')
            {
                pos++;
                tok.attribute = Enum.LCBRA;
                tok.lexeme = sub.willi(sub, pos, input);
                return tok;
            }
            else if (input.charAt(pos) == '}')
            {
                pos++;
                tok.attribute = Enum.RCBRA;
                tok.lexeme = sub.willi(sub, pos, input);
                return tok;
            }
            else if (input.charAt(pos) == ';')
            {
                pos++;
                tok.attribute = Enum.SCO;
                tok.lexeme = sub.willi(sub, pos, input);
                return tok;
            }
            else if (input.charAt(pos) == '.')
            {
                pos++;
                tok.attribute = Enum.DOT;
                tok.lexeme = sub.willi(sub, pos, input);
                return tok;
            }
            else if (input.charAt(pos) == ',')
            {
                pos++;
                tok.attribute = Enum.COM;
                tok.lexeme = sub.willi(sub, pos, input);
                return tok;
            }
        }
        else if(input.charAt(pos) == '"')
        {
            //String
            pos++;
            while(input.charAt(pos) != '"')
            {
                pos++;
            }
            pos++;
            tok = new Token(Enum.STRING_LITERAL);
            tok.lexeme = sub.willi(sub, pos, input);
            tok.lineNumber = lineNumber;
            return tok;
        }
        else if(input.charAt(pos) == '/')
        {
            pos++;
            //Comentário
            tok = new Token(Enum.DIV);
            tok.attribute = Enum.DIV;
            tok.lexeme = sub.willi(sub, pos+1, input);
            tok.lineNumber = lineNumber;
            if ((pos < input.length()) && input.charAt(pos) == '*') // Comentário de bloco
            {   
                pos++;
                tok = new Token(Enum.COMMENT);
                tok.lineNumber = lineNumber;
                while(true)
                {
                    while((pos < input.length()) && input.charAt(pos) != '*')
                    {
                        if(input.charAt(pos) == Character.LINE_SEPARATOR)
                        {
                            this.setLineNumber();
                        }
                        pos++;
                    }
                    pos++;
                    if(input.charAt(pos) == '/')
                    {
                        pos++;
                        
                        tok.lexeme = sub.willi(sub, pos+1, input);
                        return tok;
                    }
                }
                
            }
            else
            {
                if (input.charAt(pos) == '/') //Comentário de linha
                {               
                   tok = new Token(Enum.COMMENT);               
                   while ((pos < input.length()) && input.charAt(pos) != Character.LINE_SEPARATOR)
                   {                       
                       pos++;
                   }
                   sub.begin = sub.begin-2;
                   tok.lexeme = sub.willi(sub, pos+1, input);                   
                   return tok;
                }                
            }
            
        }
        return tok;
    }//Fim nextToken
    
    public void fail()
    {
        this.errors = "Erro léxico!";
    }
    
    public String getErrors()
    {
        return this.errors;
    }
    
    public void setLineNumber()
    {
        this.lineNumber++;
    }
        
    public int getLineNumber()
    {
        return this.lineNumber;
    }
}
