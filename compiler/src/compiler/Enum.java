/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package compiler;

/**
 *
 * @author Vinicios
 */
public enum Enum 
{
    CLASS,
    UNDEF,
    EXTENDS,
    CONSTRUCTOR,
    ID,
    IF,
    ELSE,
    THEN,
    OP,
    EQ,
    NE,
    GT,
    GE,
    LT,
    LE,
    AT,
    SEP,
    LPAR,
    RPAR,
    LBRA,
    RBRA,
    LCBRA,
    RCBRA,
    COM,
    SCO,
    DOT,
    STRING,
    COMMENT,
    NUMBER,
    INTEGER_LITERAL,
    FLOAT_LITERAL,
    DOUBLE_LITERAL,
    SUM,
    SUB,
    MUL,
    DIV,
    MOD,
    EOF,
    LINEEND,
    BREAK,
    PRINT,
    READ,
    NEW,
    RETURN,
    SUPER,
    FOR,
    INT,
    STRING_LITERAL,
    VAR,
    MET
}
